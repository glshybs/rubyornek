# encoding:
=begin
çok satırlı kod
=end
#tek satırlık kod
#ruby büyük küçük harfe duyarlıdır(case sensitive)
=begin
Kullaniciadi="gülşah"
kullaniciadi="farklı"
puts Kullaniciadi,kullaniciadi
değişken atanırken sırasıyla sağdaki değerler soldaki değişkenlere atanıyor
başka diller de adım adım olan atamalar örneğin
gecici=a
a=b
b=gecici
a,b=b,a
a,b=[1,2,3]
a,*b=[1,2,3]

print "Adınız"
ad=gets
print "merhaba",ad.chomp,"hoşgeldiniz"
ad.inspect
=end
#chomp : son \n karakterini siler
#chop :son karakteri siler
#dizi tanımlanırken her türden elemanı barındırır indisi  0 dan başlar.
=begin
dizi=["gülşah gök","selcan çağlar","ereğli",67,true]
puts dizi[2]
puts dizi[3]*2
dizi[-3]=20
puts dizi[-3]*8

a=25
b=50
if a>b
  puts "a, b'den büyüktür"
elsif a<b
puts "b, a'dan büyüktür"
else
  puts "sayılar birbirine eşittir"
end
# ekrana 1 defa gülah yazdırmak için 10.times {puts"gülşah"}
sayac=1
while sayac <=5
  print "#{sayac}" #değişkeni ya da metodları karakter ifadesi içine gömmek için kullanılır.
  sayac=sayac+1

end
=end
#methodlar def- end
=begin
def merhaba
  puts "merhaba dünya"
end
merhaba


def merhaba(kisi)
  puts "merhaba,#{kisi}"
end

merhaba("okur")
=begin
